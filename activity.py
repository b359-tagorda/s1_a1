name = "Berto"
age = 12
occupation = "singer"
movie = "Charlie and the Chocolate Factory"
rating = 90.99


#f-strings
print(f"I am {name} and I am {age} years old. I work as a {occupation}, and my rating for {movie} is {rating}%")


print("I am " + str(name) + " and I am " + str(age) + " years old. I work as a " + str(occupation) + ", and my rating for " + str(movie) + " is " + str(rating) + "%")


num1 = 1
num2 = 2
num3 = 3

# Get the product of num1 and num2
product = num1 * num2

# Check if num1 is less than num3
if num1 < num3:
	print("num1 is less than num3")

# Add the value of num3 to num2
num2 += num3
print("Product of num1 and num2:", product)
print("Num3 + num2:", num2)



